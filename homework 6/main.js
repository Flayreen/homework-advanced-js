const button = document.querySelector(".find-button");

button.addEventListener('click', async function(event) {
    const {ip} = await fetch("https://api.ipify.org/?format=json").then(res => res.json());
    const {country, region, city, regionName, timezone } = await fetch(`http://ip-api.com/json/${ip}`).then(res => res.json());
    const [continent] = timezone.split("/");

    const userInfo = document.querySelector(".user-info");
    userInfo.textContent = `Your location: ${continent}, ${country}, region number ${region}, ${regionName}, city ${city}`;
    document.body.prepend(userInfo)
})