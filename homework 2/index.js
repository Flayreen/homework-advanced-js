// 1. try...catch - використовується для відловлювання помилок і опрацювання їх.В цій задачі ми штучно створюємо помилку, щоб відобразати її в консолі, але ця помилка не мішає виконуватися усьому іншому коду


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function createBookList() {
    const parent = document.querySelector('#root');
    const ul = document.createElement('ul');
    parent.append(ul);

    books.forEach((obj, index) => {
        const li = document.createElement('li');
        try {
            if (!obj.author) {
                throw new Error(`Об'єкт ${index + 1} не має автора`);
            }
            if (!obj.name) {
                throw new Error(`Об'єкт ${index + 1} не має назви`);
            }
            if (!obj.price) {
                throw new Error(`Об'єкт ${index + 1} не має ціни`);
            }

            li.innerText = `Автор - ${obj.author} \n Назва - ${obj.name} \n Ціна - ${obj.price}`;
            ul.append(li);
        } catch (err) {
            console.log(err);
        }
    })
}

createBookList();






