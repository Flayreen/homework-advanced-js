// 1. Прототипне наслідування - це створення методу для функції-конструктора, яка прикріплене до конкретного класу. Тобто метод може викликатися з будь-якого обʼєкта, який сторений функціїю конструктором.
// 2. Super - це обовʼязковий метод при створенні дочірнього класу. Він собою передає конкретні аргументи і ключі з батьківського класу

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    };
    set name(value) {
        this._name = value;
        return true;
    };

    get age() {
        return this._age;
    };
    set age(value) {
        this._age = value;
        return true;
    };

    get salary() {
        return this._salary;
    };
    set salary(value) {
        this._salary = value;
        return true;
    };
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    };
}

const oleg = new Programmer('Oleg', 23, 2000, 'js, php, ruby, c++');
const vitya = new Programmer('Vitya', 33, 4000, 'pascal, c, go, rust');

console.log(oleg);
console.log(vitya);
console.log(oleg.salary)