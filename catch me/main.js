const button = document.querySelector('button');
const block = document.querySelector('.container');


function getRandomCoordinate() {
    const computedStyles = window.getComputedStyle(block);
    const min = 0;
    const maxWidth = window.innerWidth - parseInt(computedStyles.width);
    const maxHeight = window.innerHeight - parseInt(computedStyles.height);

    const randomWidth = Math.floor(Math.random() * (maxWidth - min + 1)) + min;
    const randomHeight = Math.floor(Math.random() * (maxHeight - min + 1)) + min;
    return {randomWidth, randomHeight};
}

button.addEventListener('mouseover', function (event) {
    block.style.top = getRandomCoordinate().randomHeight + 'px';
    block.style.left = getRandomCoordinate().randomWidth + 'px';
})



