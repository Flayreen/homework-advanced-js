import putUserInfo from '../API/putUserInfo.js';
import deletePost from '../API/deletePost.js';

class Post {
    constructor(title, name, email, description, id) {
        this.title = title;
        this.name = name;
        this.email = email;
        this.description = description;
        this.id = id;

        this.buttonEdit = document.createElement("button");
        this.buttonDelete = document.createElement("button");
        this.postTitle = document.createElement("span");
        this.postDescription = document.createElement("p");
    }

    render(from, renderPlace) {
        // Create post
        const postContainer = document.createElement("div");
        postContainer.classList.add("post-container");
        this.container = postContainer;
        //Create post title
        this.postTitle.classList.add("post__title");
        this.postTitle.textContent = this.title;
        //Create block with user info
        const userInfoContainer = document.createElement("div");
        userInfoContainer.classList.add("post__author-info");
        const authorName = document.createElement("span");
        authorName.classList.add("author__name");
        authorName.textContent = this.name;
        const authorMail = document.createElement("span");
        authorMail.classList.add("author__mail");
        authorMail.textContent = this.email;
        userInfoContainer.append(authorName, authorMail);
        //Create post description
        this.postDescription.classList.add("post__description");
        this.postDescription.textContent = this.description;
        //Create block with buttons
        const buttonsContainer = document.createElement("div");
        buttonsContainer.classList.add("post__buttons");
        this.buttonEdit.classList.add("button__edit");
        this.buttonEdit.textContent = "Edit";
        this.buttonDelete.classList.add("button__remove");
        this.buttonDelete.textContent = "Delete";
        buttonsContainer.append(this.buttonEdit, this.buttonDelete);
        //Contains post
        postContainer.append(this.postTitle, userInfoContainer, this.postDescription, buttonsContainer);
        //Contains list
        renderPlace.insertAdjacentElement(from, postContainer);
    }

    delete() {
        this.buttonDelete.addEventListener("click", async () => {
            try {
                const responseDelete = await deletePost(this.id);
                if (responseDelete.status === 200) {
                    const container = this.buttonDelete.closest(".post-container");
                    container.remove();
                }
            } catch (error) {
                console.log(error)
            }
        })
    }

    edit() {
        this.buttonEdit.addEventListener("click", async () => {
            try {
                // Create html markup for editor window
                document.body.style.overflow = "hidden";
                // Edit block
                const editBlock = document.createElement("div");
                editBlock.style.top = window.scrollY + "px";
                editBlock.classList.add("edit__block");
                // Edit container
                const editContainer = document.createElement("div");
                editContainer.classList.add("edit__container");
                // Edit title
                const editTitle = document.createElement("h2");
                editTitle.textContent = "Edit post";
                // Input new title
                const inputTitleBlock = document.createElement("div");
                inputTitleBlock.classList.add("edit__input-wrapper");
                const inputTitleLabel = document.createElement("label");
                inputTitleLabel.classList.add("edit__label")
                inputTitleLabel.textContent = "Post's title";
                inputTitleLabel.setAttribute("for", "inputTitle");
                const inputTitleField = document.createElement("input");
                inputTitleField.classList.add("edit__input");
                inputTitleField.value = this.title;
                inputTitleField.setAttribute("id", "inputTitle");
                inputTitleField.setAttribute("type", "text");
                inputTitleField.setAttribute("placeholder", "Enter new title");
                inputTitleBlock.append(inputTitleLabel, inputTitleField);
                // Input new description
                const inputDescriptionBlock = document.createElement("div");
                inputDescriptionBlock.classList.add("edit__input-wrapper")
                const inputDescriptionLabel = document.createElement("label");
                inputDescriptionLabel.classList.add("edit__label")
                inputDescriptionLabel.textContent = "Post's description";
                inputDescriptionLabel.setAttribute("for", "inputDescription");
                const inputDescriptionField = document.createElement("textarea");
                inputDescriptionField.value = this.description;
                inputDescriptionField.classList.add("edit__textarea");
                inputDescriptionField.setAttribute("id", "inputDescription");
                inputDescriptionField.setAttribute("placeholder", "Enter new title");
                inputDescriptionBlock.append(inputDescriptionLabel, inputDescriptionField);
                // Buttons block
                const buttonBlock = document.createElement("div");
                buttonBlock.classList.add("post__buttons")
                const buttonEdit = document.createElement("button");
                buttonEdit.classList.add("button__edit");
                buttonEdit.textContent = "Edit";
                const buttonCancel = document.createElement("button");
                buttonCancel.classList.add("button__remove");
                buttonCancel.textContent = "Cancel";
                buttonBlock.append(buttonEdit, buttonCancel)

                editContainer.append(editTitle, inputTitleBlock, inputDescriptionBlock, buttonBlock);
                editBlock.append(editContainer);
                document.body.append(editBlock);

                buttonEdit.addEventListener("click", async () => {
                    if (inputTitleField.value !== '' && inputDescriptionField.value !== '') {
                        const newInfo = {
                            newTitle: inputTitleField.value,
                            newDescription: inputDescriptionField.value,
                        }
                        const responseEdit = await putUserInfo(this.id, newInfo);

                        if (responseEdit.status === 200) {
                            this.title = newInfo.newTitle;
                            this.postTitle.textContent = this.title;
                            this.description = newInfo.newDescription;
                            this.postDescription.textContent = this.description;

                            document.body.style.overflow = "";
                            editBlock.remove();
                        }
                    }

                    if (inputTitleField.value === "" && !inputTitleField.classList.contains("input--error")) {
                        inputTitleField.classList.add("input--error");
                    }
                    if (inputDescriptionField.value === "" && !inputDescriptionField.classList.contains("input--error")) {
                        inputDescriptionField.classList.add("input--error");
                    }
                })

                buttonCancel.addEventListener("click", () => {
                    document.body.style.overflow = "";
                    editBlock.remove();
                })
            } catch (error) {
                console.log(error)
            }
        })
    }
}


export default Post;