import renderPosts from "./functions/renderPosts.js";
import createPost from "./functions/createPost.js";

const postsList = document.querySelector(".posts__wrapper");
renderPosts(postsList);

const btnCreate = document.querySelector(".button__create");
btnCreate.addEventListener("click", createPost);