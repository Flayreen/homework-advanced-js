import postPost from "../API/postPost.js";
import getPosts from "../API/getPosts.js";
import Post from "../classes/Post.js";

async function createPost() {
    // Create html markup for editor window
    document.body.style.overflow = "hidden";
    // Create block
    const createBlock = document.createElement("div");
    createBlock.style.top = window.scrollY + "px";
    createBlock.classList.add("edit__block");
    // Edit container
    const createContainer = document.createElement("div");
    createContainer.classList.add("edit__container");
    // Edit title
    const createTitle = document.createElement("h2");
    createTitle.textContent = "Create post";
    // Input new title
    const createTitleBlock = document.createElement("div");
    createTitleBlock.classList.add("edit__input-wrapper");
    const inputTitleLabel = document.createElement("label");
    inputTitleLabel.classList.add("edit__label")
    inputTitleLabel.textContent = "Post's title";
    inputTitleLabel.setAttribute("for", "inputTitle");
    const inputTitleField = document.createElement("input");
    inputTitleField.classList.add("edit__input");
    inputTitleField.setAttribute("id", "inputTitle");
    inputTitleField.setAttribute("type", "text");
    inputTitleField.setAttribute("placeholder", "Enter post's title");
    createTitleBlock.append(inputTitleLabel, inputTitleField);
    // Input new description
    const createDescriptionBlock = document.createElement("div");
    createDescriptionBlock.classList.add("edit__input-wrapper")
    const inputDescriptionLabel = document.createElement("label");
    inputDescriptionLabel.classList.add("edit__label")
    inputDescriptionLabel.textContent = "Post's description";
    inputDescriptionLabel.setAttribute("for", "inputDescription");
    const inputDescriptionField = document.createElement("textarea");
    inputDescriptionField.classList.add("edit__textarea");
    inputDescriptionField.setAttribute("id", "inputDescription");
    inputDescriptionField.setAttribute("placeholder", "Enter post's description");
    createDescriptionBlock.append(inputDescriptionLabel, inputDescriptionField);
    // Buttons block
    const buttonsBlock = document.createElement("div");
    buttonsBlock.classList.add("post__buttons")
    const buttonCreate = document.createElement("button");
    buttonCreate.classList.add("button__edit");
    buttonCreate.textContent = "Create";
    const buttonCancel = document.createElement("button");
    buttonCancel.classList.add("button__remove");
    buttonCancel.textContent = "Cancel";
    buttonsBlock.append(buttonCreate, buttonCancel);

    createContainer.append(createTitle, createTitleBlock, createDescriptionBlock, buttonsBlock);
    createBlock.append(createContainer);
    document.body.append(createBlock);


    buttonCreate.addEventListener("click", async () => {
        if (inputTitleField.value !== '' && inputDescriptionField.value !== '') {
            const posts = await getPosts();
            const postsList = document.querySelector(".posts__wrapper");

            const newPostInfo = {
                title: inputTitleField.value,
                body: inputDescriptionField.value,
                userId: 1,
                id: ++posts.length,
            }
            const responseEdit = await postPost(newPostInfo);

            const newPost = new Post(inputTitleField.value, "Leanne Graham", "Sincere@april.biz", inputDescriptionField.value, ++posts.length);
            newPost.render("afterbegin", postsList);
            newPost.delete();
            newPost.edit();

            document.body.style.overflow = "";
            createBlock.remove();
        }

        if (inputTitleField.value === "" && !inputTitleField.classList.contains("input--error")) {
            inputTitleField.classList.add("input--error");
        }
        if (inputDescriptionField.value === "" && !inputDescriptionField.classList.contains("input--error")) {
            inputDescriptionField.classList.add("input--error");
        }
    })

    buttonCancel.addEventListener("click", () => {
        document.body.style.overflow = "";
        createBlock.remove();
    })
}

export default createPost;