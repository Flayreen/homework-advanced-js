import Post from "../classes/Post.js";
import getUsers from "../API/getUsers.js";
import getPosts from "../API/getPosts.js";

async function renderPosts(postsLocation) {
    try {
        // http
        const users = await getUsers();
        const posts = await getPosts();

        // Render posts
        posts.forEach(({userId, title, body, id: postId}) => {
            users.forEach(({id, name, email}) => {
                if (userId === id) {
                    const newPost = new Post(title, name, email, body, postId);
                    newPost.render("beforeend", postsLocation);
                    newPost.delete();
                    newPost.edit();
                }
            })
        })
        // Remove loader
        const preloader = document.querySelector(".loader");
        preloader.remove();
    } catch (err) {
        console.log(err)
    }
}

export default renderPosts;