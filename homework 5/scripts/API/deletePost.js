async function deletePost(id) {
    try {
        const responseDelete = await axios.delete(`https://ajax.test-danit.com/api/json/posts/${id}`);
        return responseDelete;
    } catch (err) {
        console.log(err)
    }
}

export default deletePost;