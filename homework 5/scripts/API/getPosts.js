async function getPosts() {
    try {
        const posts = await axios.get("https://ajax.test-danit.com/api/json/posts").then(res => res.data);
        return posts;
    } catch (err) {
        console.log(err)
    }
}

export default getPosts;