async function putUserInfo(id, newInfo) {
    try {
        const responseEdit = await axios.put(`https://ajax.test-danit.com/api/json/posts/${id}`, {
            body: newInfo,
        });
        return responseEdit;
    } catch (err) {
        console.log(err)
    }
}

export default putUserInfo;