async function getUsers() {
    try {
        const users = await axios.get("https://ajax.test-danit.com/api/json/users").then(res => res.data);
        return users;
    } catch (err) {
        console.log(err)
    }
}

export default getUsers;