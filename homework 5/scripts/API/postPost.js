async function postPost(data) {
    try {
        const response = await axios.post("https://ajax.test-danit.com/api/json/posts", data).then(res => res.data);
        return response;
    } catch (err) {
        console.log(err)
    }
}

export default postPost;