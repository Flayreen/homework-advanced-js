const days = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const monthNames = [
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

const date = new Date();
const hour = document.querySelector('.timer__hour');
hour.value = String(date.getHours()).length < 2 ? `0${date.getHours()}` : date.getHours();

const minute = document.querySelector('.timer__minute');
minute.value = String(date.getMinutes()).length < 2 ? `0${date.getMinutes()}` : date.getMinutes();

const second = document.querySelector('.timer__seconds');
second.value = String(date.getSeconds()).length < 2 ? `0${date.getSeconds()}` : date.getSeconds();

const day = document.querySelector('.timer__day');
day.textContent = `${days[date.getDay()]}, ${monthNames[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`;







setInterval(function () {
    const date = new Date();
    const liveSeconds = date.getSeconds();
    const liveMinutes = date.getMinutes();
    const liveHours = date.getHours();


    second.value = String(liveSeconds).length < 2 ? `0${liveSeconds}` : liveSeconds;
    minute.value = String(liveMinutes).length < 2 ? `0${liveMinutes}` : liveMinutes;
    hour.value = String(liveHours).length < 2 ? `0${liveHours}` : liveHours;

    const liveDay = days[date.getDay()];
    const liveMonth = monthNames[date.getMonth()];
    const liveDate = date.getDate();
    const liveYear = date.getFullYear();
    day.textContent = `${liveDay}, ${liveMonth} ${liveDate} ${liveYear}`
}, 1000)

