const filmBlock = document.querySelector(".film__container");
filmBlock.remove();
const container = document.querySelector(".wrapper");

// fetch("https://ajax.test-danit.com/api/swapi/films")
//     .then(res => res.json())
//     .then(data => {
//         data.map(({name, episodeId, openingCrawl, characters}) => {
//
//             const filmBlockCopy = filmBlock.cloneNode(true);
//             const filmTitle = filmBlockCopy.querySelector('.film__title');
//             const filmEpisode = filmBlockCopy.querySelector('.info__description--episode');
//             const filmPlot = filmBlockCopy.querySelector('.info__description--crawl');
//             const filmActors = filmBlockCopy.querySelector('.info__description--characters');
//             container.append(filmBlockCopy);
//
//             filmTitle.textContent = name;
//             filmEpisode.textContent = episodeId;
//             filmPlot.textContent = openingCrawl;
//
//             const actorFetchPromises = characters.map(actorApi =>
//                 fetch(actorApi)
//                     .then(res => res.json())
//                     .then(actor => actor.name)
//             );
//
//             Promise.all(actorFetchPromises)
//                 .then(actor => {
//                     filmActors.innerHTML = actor.join(", ");
//                 })
//         });
//
//     })
//     .catch(error => {
//         console.error(error);
//     });


async function getInfo() {
    try {
        const info = await fetch("https://ajax.test-danit.com/api/swapi/films").then(res => res.json());
        info.map(({name, episodeId, openingCrawl, characters}) => {
            const filmBlockCopy = filmBlock.cloneNode(true);
            container.append(filmBlockCopy);

            const filmTitle = filmBlockCopy.querySelector('.film__title');
            const filmEpisode = filmBlockCopy.querySelector('.info__description--episode');
            const filmPlot = filmBlockCopy.querySelector('.info__description--crawl');
            const filmActors = filmBlockCopy.querySelector('.info__description--characters');
            filmTitle.textContent = name;
            filmEpisode.textContent = episodeId;
            filmPlot.textContent = openingCrawl;

            const actorFetchPromises = characters.map(actorApi =>
                fetch(actorApi)
                    .then(res => res.json())
                    .then(actor => actor.name)
            );

            Promise.all(actorFetchPromises)
                .then(actor => {
                    filmActors.innerHTML = actor.join(", ");
                })
        });
    } catch (err) {
        console.log(err)
    }
}

getInfo();
