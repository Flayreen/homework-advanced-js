const kittenBlock = document.querySelector('.kitten__block');
const inputBlock = document.querySelector('.kitten__input-block');
const kittenInput = document.querySelector('.kitten__input');
const kittenText = document.querySelector('.kitten__text');

inputBlock.style.width = getComputedStyle(kittenInput).width;

const textField = document.querySelector('.creator__textarea');
const button = document.querySelector('.creator__button');

button.addEventListener('click', function (event) {

    if (textField.value !== '') {
        const cloneKitten = kittenBlock.cloneNode(true);
        const textKitten = cloneKitten.querySelector('.kitten__text');
        const inputKitten = cloneKitten.querySelector('.kitten__input-block');
        inputKitten.style.width = getComputedStyle(kittenInput).width;
        textKitten.textContent = textField.value;
        cloneKitten.style.opacity = '100%';
        cloneKitten.style.top = '100px';
        cloneKitten.style.right = '-' + (parseInt(getComputedStyle(kittenBlock).width) + parseInt(getComputedStyle(document.body).width)) + 'px';

        document.body.prepend(cloneKitten);

    }
})
