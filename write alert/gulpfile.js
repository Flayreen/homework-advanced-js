import pkg from 'gulp';
const { task, parallel, watch, series, lastRun, src, dest } = pkg;

// Пакети для стилів
import gulpSass from 'gulp-sass';
import * as dartSass from 'sass';
const sass = gulpSass(dartSass);
import minifyCSS from 'gulp-clean-css';

// Пакети для HTML
import fileInclude from 'gulp-file-include';
import prettyHtml from 'gulp-pretty-html';

// Інші пакети
import { deleteAsync } from 'del';
import browserSync from 'browser-sync';



task('cleanDist', function () {
    return deleteAsync(["./dist"]);
})

task('styleCSS', function () {
    return src('src/styles/*.scss')
        .pipe(sass.sync({
            sourceComments: false,
            outputStyle: "expanded"
        }).on('error', sass.logError))
        .pipe(minifyCSS({ compatibility: 'ie8' }))
        .pipe(dest('./dist/css/'))
        .pipe(browserSync.stream())
});

task('moveJS', function () {
    return src('src/scripts/**/*.js')
        .pipe(dest('dist/js'))
        .pipe(browserSync.stream())
})

task('moveHTML', function () {
    return src('src/view/*.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.stream())
})

task('moveIMG', function () {
    return src('./src/img/**/*')
        .pipe(dest('./dist/img'))
        .pipe(browserSync.stream())
})

task('server',  function () {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });
})

task('watcher', function () {
    watch('./src/styles/**/*.scss', parallel('styleCSS')).on('change', browserSync.reload);
    watch('./src/scripts/**/*.js', parallel('moveJS')).on('change', browserSync.reload);
    watch('./src/view/*.html', parallel('moveHTML')).on('change', browserSync.reload);
    watch('./src/img/**/*', parallel('moveIMG')).on('change', browserSync.reload);
})



task('dev', series('cleanDist', 'styleCSS', 'moveJS', 'moveHTML', 'moveIMG'));

task('build', series('moveHTML', 'styleCSS', 'moveJS', 'moveIMG'));

task('default', parallel('dev', 'watcher', "server"));
