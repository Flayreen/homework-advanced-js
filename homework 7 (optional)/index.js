const inactiveCells = document.getElementsByClassName('inactive--cell');
const activeCells = document.getElementsByClassName('active--cell');
const buttons = document.querySelector('.difficulty__buttons');


class Game {
    constructor(inactiveCells, activeCells, buttons) {
        this.inactiveCells = inactiveCells;
        this.activeCells = activeCells;
        this.computerCounter = 0;
        this.playerCounter = 0;
        this.difficulty = 0;
        this.buttons = buttons;
    }

    start() {
        const powerButtons = document.querySelectorAll('.power__button');

        this.buttons.addEventListener('click', (event) => {
            const targetButton = event.target.closest('.power__button');
            if (targetButton) {
                if (targetButton.textContent.toLowerCase() === 'easy') {
                    this.difficulty = 1500;
                }
                if (targetButton.textContent.toLowerCase() === 'medium') {
                    this.difficulty = 1000;
                }
                if (targetButton.textContent.toLowerCase() === 'hard') {
                    this.difficulty = 500;
                }

                powerButtons.forEach(button =>{
                    button.setAttribute('disabled', '');
                    button !== targetButton ? button.classList.add('power__button--disable') : true
                })

                openCell();
            }
        })

        const getRandomIndex = () => {
            const min = 0;
            const max = this.inactiveCells.length - 1;
            const randomIndex = Math.floor(Math.random() * (max - min + 1)) + min;
            return randomIndex;
        }


        const openCell = () => {
            const computerCounter = document.querySelector('.counter__computer-amount');
            const playerCounter = document.querySelector('.counter__player-amount');

            if (this.inactiveCells.length === 0) {
                if (this.computerCounter > this.playerCounter) {
                    alert('Компуктер виграв!');
                    return location.reload();
                }

                if (this.computerCounter < this.playerCounter) {
                    alert('Вітаю, ти виграв!');
                    return location.reload();
                }

                if (this.computerCounter === this.playerCounter) {
                    alert('Лол нічия');
                    return location.reload();
                }
            }

            const focusCell = this.inactiveCells[getRandomIndex()];
            focusCell.classList.add('cell--temporary');

            const addGreenColor= () => {
                focusCell.classList.remove('cell--temporary', 'inactive--cell');
                focusCell.classList.add('cell--player', 'active--cell');
                focusCell.removeEventListener('click', addGreenColor);
                this.playerCounter++;
                playerCounter.textContent = this.playerCounter
                openCell();
            };
            focusCell.addEventListener('click', addGreenColor);

            const addRedColor = () => {
                if (!focusCell.classList.contains('cell--player')) {
                    focusCell.classList.remove('cell--temporary', 'inactive--cell');
                    focusCell.classList.add('cell--computer', 'active--cell');
                    focusCell.removeEventListener('click', addGreenColor);
                    this.computerCounter++;
                    computerCounter.textContent = this.computerCounter;
                    openCell();
                }
            }
            setTimeout(addRedColor, this.difficulty);
        }
    }
}

const newGame = new Game(inactiveCells, activeCells, buttons);
newGame.start();


