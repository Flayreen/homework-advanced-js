class Calculator {
    constructor() {
        this.display = document.querySelector('.display--input')
        this.currentInput = '';
        this.operator = '';
        this.result = '';
        this.memory = '';
        this.memorySign = document.querySelector('.memory');
        this.memoryRecallClickCount = 0;
    }

    appendValue(value) {
        this.memorySign.style.display = 'none';
        this.currentInput += value;
        this.display.value = this.currentInput;
    }

    appendOperator(operator) {
        if (this.currentInput !== '' && this.operator === '') {
            this.memorySign.style.display = 'none';
            this.operator = operator;
            this.result = this.currentInput;
            this.currentInput = '';
        }
    }

    getResult() {
        if (this.currentInput !== '' && this.operator !== '' && this.result !== '') {
            this.memorySign.style.display = 'none';
            switch (this.operator) {
                case "+":
                    this.result = Number(this.result) + Number(this.currentInput);
                    break;
                case "-":
                    this.result = Number(this.result) - Number(this.currentInput);
                    break;
                case "*":
                    this.result = Number(this.result) * Number(this.currentInput);
                    break;
                case "/":
                    this.result = Number(this.result) / Number(this.currentInput);
                    break;
            }
            this.display.value = this.result;
            this.currentInput = this.result;
            this.operator = '';
        }
    }

    clear() {
        this.memorySign.style.display = 'none';
        this.display.value = '';
        this.currentInput = '';
        this.operator = '';
        this.result = '';
    }

    memoryPlus() {
        this.memorySign.style.display = 'inline';
        if (this.memory === '') {
            this.memory = +this.display.value;
        } else {
            this.memory = Number(this.memory) + Number(this.display.value);
            this.display.value = this.memory;
        }
    }

    memoryMinus() {
        this.memorySign.style.display = 'inline';
        if (this.memory === '') {
            this.memory = -this.display.value;
        } else {
            this.memory = Number(this.memory) - Number(this.display.value);
            this.display.value = this.memory;
        }
    }

    memoryReadAndClear() {
        if (this.memory !== '') {
            if (this.memoryRecallClickCount === 0) {
                this.memorySign.style.display = 'inline';
                this.display.value = this.memory;
                this.memoryRecallClickCount++;
            } else {
                this.memorySign.style.display = 'none';
                this.currentInput = '';
                this.memory = '';
                this.display.value = '';
                this.memoryRecallClickCount = 0;
            }
        }
    }
}

const calculator = new Calculator();

const calculatorBox = document.querySelector('.box');
calculatorBox.addEventListener('click', function (event) {
    const operator = event.target.closest('.pink');
    if (operator) {
        calculator.appendOperator(operator.value)
    }

    const number = event.target.closest('.number');
    if (number) {
        calculator.appendValue(number.value)
    }

    const result = event.target.closest('.orange');
    if (result) {
        calculator.getResult();
    }

    const clear = event.target.closest('input[value="C"]')
    if (clear) {
        calculator.clear();
    }

    const memoryPlus = event.target.closest('input[value="m+"]');
    if (memoryPlus) {
        calculator.memoryPlus();
        console.log(calculator.memory)
    }

    const memoryMinus = event.target.closest('input[value="m-"]');
    if (memoryMinus) {
        calculator.memoryMinus();
        console.log(calculator.memory)
    }

    const memoryRecall = event.target.closest('input[value="mrc"]');
    if (memoryRecall) {
        calculator.memoryReadAndClear();
    }
})


document.addEventListener('keydown', function (event) {
    if (event.key === '1' || event.key === '2' || event.key === '3' || event.key === '4' || event.key === '5' || event.key === '6' || event.key === '7' || event.key === '8' || event.key === '9' || event.key === '0' || event.key === '.') {
        calculator.appendValue(event.key)
    }

    if (event.key === '+' || event.key === '-' || event.key === '*' || event.key === '/') {
        calculator.appendOperator(event.key)
    }

    if (event.code === 'Enter') {
        calculator.getResult();
    }

    if (event.key === 'Backspace') {
        calculator.clear();
    }
})