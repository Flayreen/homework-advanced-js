// Task 3
const user1 = {
    name: "John",
    years: 30
};

const {name: userName, years: userYears, isAdmin = false} = user1;
const name = userName;
const age = userYears;
const admin = isAdmin;
console.log(name)
console.log(age)
console.log(admin)
