
// Task 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

function createDataBase(firstBase, secondBase) {
    const newClients = secondBase.filter(secondValue => !firstBase.includes(secondValue));
    return [...firstBase, ...newClients];
}
console.log(createDataBase(clients1, clients2))



// За допомогою new Set я видаляю дуплікати, більш лаконічний спосіб
const newBase = [...new Set([...clients1, ...clients2])]
console.log(newBase)

